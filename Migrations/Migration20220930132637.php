<?php declare(strict_types=1);
/**
 * @package Plugin\jtl_tmrapi\Migrations
 * @author  Felix Moche
 */

namespace Plugin\jtl_tmrapi\Migrations;

use JTL\Plugin\Migration;
use JTL\Update\IMigration;

/**
 * Class Migration20220930132637
 * @package Plugin\jtl_tmrapi\Migrations
 */
class Migration20220930132637 extends Migration implements IMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `jtlapitestlocalization` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `languageID` INT NOT NULL,
  `itemid` INT NOT NULL,
  `text` MEDIUMTEXT NOT NULL,
  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->execute('DROP TABLE IF EXISTS `jtlapitestlocalization`');
    }
}
