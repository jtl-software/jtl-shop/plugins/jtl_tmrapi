<?php declare(strict_types=1);

namespace Plugin\jtl_tmrapi\Models;

use Exception;
use Illuminate\Support\Collection;
use JTL\Model\DataAttribute;
use JTL\Model\DataModel;
use JTL\Plugin\Admin\InputType;
use JTL\Shop;

/**
 * Class ApitestModel
 * @package Plugin\jtl_tmrapi\Models
 * @property int    $id
 * @method int getId()
 * @method void setId(int $value)
 * @property string $name
 * @method string getName()
 * @method void setName(string $value)
 * @property string $description
 * @method string getDescription()
 * @method void setDescription(string $value)
 * @method void setUrl(string $value)
 * @method Collection getCurrentLocalization()
 * @method Collection getLocalization()
 */
final class ApitestModel extends DataModel
{
    /**
     * @inheritdoc
     */
    public function getTableName(): string
    {
        return 'jtlapitest';
    }

    /**
     * Setting of keyname is not supported!
     * Call will always throw an Exception with code ERR_DATABASE!
     * @inheritdoc
     */
    public function setKeyName($keyName): void
    {
        throw new Exception(__METHOD__ . ': setting of keyname is not supported', self::ERR_DATABASE);
    }

    /**
     * @inheritDoc
     */
    protected function onRegisterHandlers(): void
    {
        $this->registerSetter('localization', function ($value, $model) {
            if (\is_a($value, Collection::class) || $value === null) {
                return $value;
            }
            if (!\is_array($value)) {
                $value = [$value];
            }
            $res = $model->localization ?? new Collection();
            foreach (\array_filter($value) as $data) {
                if (empty($data['itemid'])) {
                    $data['itemid'] = $model->id;
                }
                try {
                    $loc = ApitestLocalizationModel::loadByAttributes(
                        $data,
                        $this->getDB(),
                        self::ON_NOTEXISTS_NEW
                    );
                } catch (Exception) {
                    continue;
                }
                $existing = $res->first(static function ($e) use ($loc): bool {
                    return $loc->id > 0 && $e->id === $loc->id;
                });
                if ($existing === null) {
                    $res->push($loc);
                } else {
                    foreach ($loc->getAttributes() as $attribute => $v) {
                        if (\array_key_exists($attribute, $data)) {
                            $existing->setAttribValue($attribute, $loc->getAttribValue($attribute));
                        }
                    }
                }
            }

            return $res;
        });
        $this->registerGetter('currentLocalization', function () {
            $currentLang = Shop::getLanguageID();
            return $this->getLocalization()->first(function (ApitestLocalizationModel $e) use ($currentLang) {
                return $e->getLanguageID() === $currentLang;
            });
        });
    }

    /**
     * @inheritdoc
     */
    public function getAttributes(): array
    {
        static $attributes = null;
        if ($attributes === null) {
            $attributes       = [];
            $attributes['id'] = DataAttribute::create('id', 'int', null, false, true);
            $attributes['id']->getInputConfig()->setModifyable(false);
            $attributes['name']        = DataAttribute::create('name', 'varchar', null, false);
            $attributes['description'] = DataAttribute::create('description', 'mediumtext', null, false);
            $attributes['description']->getInputConfig()->setInputType(InputType::TEXTAREA);
            $attributes['localization'] = DataAttribute::create(
                'localization',
                ApitestLocalizationModel::class,
                null,
                true,
                false,
                'id',
                'itemid'
            );

            $attributes['url'] = DataAttribute::create('url', 'varchar', null, false, false, null, null, true);
            $attributes['url']->getInputConfig()->setHidden(true);

            $attributes['currentLocalization'] = DataAttribute::create(
                'currentLocalization',
                'varchar',
                null,
                false,
                false,
                null,
                null,
                true
            );
            $attributes['currentLocalization']->getInputConfig()->setHidden(true);
        }

        return $attributes;
    }
}
