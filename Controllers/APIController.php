<?php declare(strict_types=1);

namespace Plugin\jtl_tmrapi\Controllers;

use Exception;
use Illuminate\Support\Collection;
use JTL\Catalog\Navigation;
use JTL\Catalog\NavigationEntry;
use JTL\Model\DataModelInterface;
use JTL\Plugin\PluginInterface;
use JTL\Router\Controller\AbstractController;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Laminas\Diactoros\Response\JsonResponse;
use Plugin\jtl_tmrapi\Models\ApitestModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 *
 */
class APIController extends AbstractController
{
    private ?NavigationEntry $navigationEntry = null;

    public function show(ServerRequestInterface $request, array $args, JTLSmarty $smarty): ResponseInterface
    {
        $itemID = (int)($args['id'] ?? -1);
        if ($itemID > 0) {
            try {
                $model = ApitestModel::loadByAttributes(['id' => $itemID], $this->db);
            } catch (Exception) {
                return new JsonResponse('', 404);
            }

            return new JsonResponse($model->rawArray(true));
        }
        $models = ApitestModel::loadAll($this->db, [], []);

        return new JsonResponse($models->map(static function (DataModelInterface $item) {
            return $item->rawArray(true);
        })->toArray());
    }

    public function update(ServerRequestInterface $request, array $args, JTLSmarty $smarty): ResponseInterface
    {
        $itemID = (int)$args['id'];
        try {
            $model = ApitestModel::loadByAttributes(['id' => $itemID], $this->db);
        } catch (Exception) {
            return new JsonResponse('Could not find item', 500);
        }
        $body    = $request->getParsedBody();
        $updates = [];
        foreach ($model->getAttributes() as $attribute) {
            $name = $attribute->getName();
            if (isset($body[$name])) {
                $updates[] = $name;
                $model->setAttribValue($name, $body[$name]);
            }
        }
        if ($model->save($updates)) {
            return new JsonResponse($model->rawArray(true));
        }

        return new JsonResponse('Could not update item', 500);
    }

    public function create(ServerRequestInterface $request, array $args, JTLSmarty $smarty): ResponseInterface
    {
        $model = new ApitestModel($this->db);
        $body  = $request->getParsedBody();
        foreach ($model->getAttributes() as $attribute) {
            $name = $attribute->getName();
            if (isset($body[$name])) {
                $model->setAttribValue($name, $body[$name]);
            }
        }
        if ($model->save()) {
            return new JsonResponse($model->rawArray(true));
        }

        return new JsonResponse('Could not create item', 500);
    }

    public function delete(ServerRequestInterface $request, array $args, JTLSmarty $smarty): ResponseInterface
    {
        $itemID = (int)$args['id'];
        try {
            $model = ApitestModel::loadByAttributes(['id' => $itemID], $this->db);
            $model->delete();
        } catch (Exception) {
            return new JsonResponse('Could not delete item', 500);
        }

        return new JsonResponse('', 204);
    }

    protected function getNavigation(): Navigation
    {
        $nav = parent::getNavigation();
        if ($this->navigationEntry !== null) {
            $nav->setCustomNavigationEntry($this->navigationEntry);
        }

        return $nav;
    }

    public function getResponse(ServerRequestInterface $request, array $args, JTLSmarty $smarty): ResponseInterface
    {
        // NOTICE:
        // these will not work with language switcher if  FLAT url structures without locale code are used
        // in this case you should create links with a slug from the JtlapitestLocalization table
        $this->smarty      = $smarty;
        $model             = null;
        $this->currentLink = $this->config['link'];
        Shop::setPageType(\PAGE_PLUGIN);
        if (!empty($args['slug'])) {
            try {
                $model = ApitestModel::loadByAttributes(['name' => \urldecode($args['slug'])], $this->db);
//                $data = ApitestLocalizationModel::loadByAttributes(['text' => \urldecode($args['slug'])], $this->db);
//                $model = ApitestModel::loadByAttributes(['id' => $data->itemid], $this->db);
//                if ($data->getLanguageID() !== Shop::getLanguageID()) {
//                    Shop::updateLanguage($data->getLanguageID());
//                }
            } catch (Exception) {
                return $this->notFoundResponse($request, $args, $smarty);
            }
        }
        $state                   = Shop::getRouter()->getState();
        $state->currentRouteName = 'tmaItemList';
        if ($model !== null) {
            $state->itemID         = $model->getId();
            $state->routeData      = [
                'id'   => $model->getId(),
                'slug' => $model->getName()
            ];
            $this->navigationEntry = new NavigationEntry();
            $this->navigationEntry->setName($model->getName());
            $this->navigationEntry->setURL('');
            $this->navigationEntry->setURLFull('');
        }
        $this->init();
        $this->preRender();

        return $this->smarty->assign('Link', $this->currentLink)
            ->assign('model', $model)
            ->assign('models', $this->getModels())
            ->assign('cPluginTemplate', __DIR__ . '/../frontend/item.tpl')
            ->getResponse('layout/index.tpl');
    }

    private function getModels(): Collection
    {
        $langID = Shop::getLanguageID();
        $router = Shop::getRouter();
        $locale = $this->getLocaleFromLanguageID($langID);
        return ApitestModel::loadAll($this->db, [], [])->each(function (ApitestModel $e) use ($router, $locale, $langID) {
            // NOTICE
            // this could be used for getting a localized model
//            $localization = $e->getLocalization()->firstWhere(function (JtlapitestLocalizationModel $i) use ($langID) {
//                return $langID === $i->getLanguageID();
//            });
//            $e->setUrl($router->getURLByType(
//                'tmaItemList',
//                ['slug' => $localization->getText(), 'id' => $e->getId(), 'lang' => $locale],
//                true,
//                true
//            ));
            $e->setUrl($router->getURLByType(
                'tmaItemList',
                ['slug' => $e->getName(), 'id' => $e->getId(), 'lang' => $locale],
                true,
                true
            ));
        });
    }

}
